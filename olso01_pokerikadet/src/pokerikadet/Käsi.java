package pokerikadet;
/**
*
* @author Anita Petunova 16.1.2020
*/
import java.util.Arrays;

public class Käsi {
	
    final static int EIVÄRI = 0, EISUORA = 0, SUORA = 1, VÄRI = 2, VÄRISUORA = 3;
    // Näiden näkyvyys: pakkauksen sisällä (ei private, ei public)
    // Viittaus muista luokista muodossa Luokka.tunnus esim. Käsi.SUORA

    // Esittele instanssimuuttujat
    private Kortti[] cards = new Kortti[5];

    // Kirjoita konstruktori, joka saa parametrina Pakka-olion
  //  Pakka pakka = new Pakka();
   
    Käsi (Pakka pakka) {
    	for (int i = 0; i <= 4; i++) {
    	cards[i] = pakka.annaKortti();
    	//System.out.println(cards[i]);
    	}
    }
    
    Käsi (Kortti[] cards) {
    	this.cards = cards;
    }    
    // Kirjoita toteutus metodille annaArvo()

    int annaArvo() {
    	int maa1 = cards[0].getMaa(), maa2 = cards[1].getMaa(), maa3 = cards[2].getMaa(),
    			maa4 = cards[3].getMaa(), maa5 = cards[4].getMaa();
    	
    	int arvo1 = cards[0].getArvo(), arvo2 = cards[1].getArvo(), arvo3 = cards[2].getArvo(),
    			arvo4 = cards[3].getArvo(), arvo5 = cards[4].getArvo();
    	
    	// MAX ARVO
    	int max;
    	if (arvo1 > arvo2 && arvo1 > arvo3 && arvo1 > arvo4 && arvo1 > arvo4) {
    		max = arvo1;
    	}    	
	    	else if (arvo2 > arvo1 && arvo2 > arvo3 && arvo2 > arvo4 && arvo2 > arvo4) {
	    		max = arvo2;
	    	}
		    	else if (arvo3 > arvo1 && arvo3 > arvo2 && arvo3 > arvo4 && arvo3 > arvo5) {
		    		max = arvo3;
		    	}
			    	else if (arvo4 > arvo1 && arvo4 > arvo2 && arvo4 > arvo3 && arvo4 > arvo5) {
			    		max = arvo4;
			    	}
		else {
			max = arvo5;
		}
    	//System.out.println("max = " + max);
    	// MIN ARVO
    	int min;
    	if (arvo1 < arvo2 && arvo1 < arvo3 && arvo1 < arvo4 && arvo1 < arvo5) {
    		min = arvo1;
    	}    	
	    	else if (arvo2 < arvo1 && arvo2 < arvo3 && arvo2 < arvo4 && arvo2 < arvo5) {
	    		min = arvo2;
	    	}
		    	else if (arvo3 < arvo1 && arvo3 < arvo2 && arvo3 < arvo4 && arvo3 < arvo5) {
		    		min = arvo3;
		    	}
			    	else if (arvo4 < arvo1 && arvo4 < arvo2 && arvo4 < arvo3 && arvo4 < arvo5) {
			    		min = arvo4;
			    	}
		else {
			min = arvo5;
		}
    	
    	//System.out.println("min = " + min);
    	int emaxmin = max - min;
    	    	
    	if (maa1 == maa2 && maa1 == maa3 && maa1 == maa4 && maa1 == maa5 
    			&& emaxmin == 4 && arvo1 != arvo2 && arvo2 != arvo3 && arvo3 != arvo4 && arvo4 != arvo5) {
    		return 3;
    	}
    	
	    	else if (maa1 == maa2 && maa1 == maa3 && maa1 == maa4 && maa1 == maa5 ) {
	    		return 2;
	    	}    	 
	    	
		    	else if ((emaxmin == 4 && arvo1 != arvo2 && arvo2 != arvo3 && arvo3 != arvo4 && arvo4 != arvo5) ||
		    	(emaxmin == 12 && arvo1 != arvo2 && arvo2 != arvo3 && arvo3 != arvo4 && arvo4 != arvo5)){
		    		return 1;
	    	}    		
	    	
    	else {
    		return 0;
    	}
    }
    
    // Kirjoita toString()
    
    public String toString() {
    	return cards[0] + ", " + cards[1] + ", " + cards[2] + ", " + cards[3] + ", " + cards[4];
    }
}
