package pokerikadet;
/**
*
* @author Anita Petunova 15.1.2020
*/
public class Pakka {

    final static int KORTTILKM = 52;
	// Näiden näkyvyys: pakkauksen sisällä (ei private, ei public)
    // Viittaus muista luokista Luokka.tunnus, esim. Pakka.KORTTILKM

    // Esittele instanssimuuttujat
  Kortti[] kortit = new Kortti[53];
  int vuorossa = 1;

    // Kirjoita konstruktori, joka alustaa pakan Kortti-olioilla
   Pakka() {
    	int k=1;
    	for (int i = 0; i < 4; i++) {
    		for (int j = 1; j <= 13; j++) {
    			kortit[k] = new Kortti(i,j);
    			k++;    			
    		}
    	}
    }     
   // Kirjoita metodi sekoita()    	
 void sekoita() {
	 for (int k = 0; k < 100; k++) {
		 int one = (int) (Math.random()*52+1);
		 int two = (int) (Math.random()*52+1);

		 while (one == two) {
			 one = (int) (Math.random()*52+1);
		 }
		 
		 Kortti seka = kortit[one];
		 kortit[one] = kortit[two];
		 kortit[two]= seka;		 
	 }
 }
 
   Kortti annaKortti() {
	   if (vuorossa == 53) {
		   vuorossa = 1;
	   }
	
	   return kortit[vuorossa++];
   }
   
}
