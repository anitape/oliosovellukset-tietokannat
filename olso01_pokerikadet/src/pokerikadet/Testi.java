package pokerikadet;
/**
*
* @author Anita Petunova 16.1.2020
*/
public class Testi {

    public static void main(String[] args) {
    	Kortti[] cards = new Kortti[4];
    	// Luo pakka
    	Pakka pakka = new Pakka();
    	Käsi hand = new Käsi(pakka);
    	int vari = hand.annaArvo();    	
    	
    	int kertoja = 0;
    	do {   	
    	pakka.sekoita(); // Sekoita pakka  	
	    	for (int i = 0; i <= 4; i++) {
	        	cards[i] = pakka.annaKortti(); // Jaa käsi
	        	}
    	hand.toString(); // Näytä (tulosta) käsi     	   	
    	hand.annaArvo(); // Tarkasta onko väri
    	kertoja++;
    	} while (vari != 2);
    	
    	System.out.println(kertoja);    	
    }
}
