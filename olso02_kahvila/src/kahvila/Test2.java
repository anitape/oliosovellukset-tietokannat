package kahvila;

import org.junit.jupiter.api.Test;

public class Test2 {
	
	@Test
    public void test_tehtava7() {		
		
	// Esittele instanssimuuttujat
	int tuotenumero;
    Valikoima valikoima = new Valikoima();
    
    //Luo 3 tuotetta
    Tuote[] tuotteita = {
        new Tuote("Ystävänpäivän munkki", 1.0), // tuotenumero = 1
        new Tuote("Runebergin torttu", 2.55), // tuotenumero = 2
        new Tuote("Dallaspulla", 2.25) // tuotenumero = 3
    };   
    
    //Lisää tuotteet valikoimaan
    for (Tuote tuote : tuotteita) {
		valikoima.lisääTuoteValikoimaan(tuote);
	}
    
    //Tulostaa valikoiman
    System.out.println("Tuotteet:");
	valikoima.tulostaTuotteet();
	
	// Poista siitä yksi tuote.
	int poistettavaTuote;
	poistettavaTuote = (int) (Math.random()*tuotteita.length) +1 ;
	System.out.println("\nPoistettavan tuotteen tuotenumero on: " + poistettavaTuote);
	valikoima.poistaTuoteValikoimasta(poistettavaTuote);
	
	//Tulosta muuttunut valikoima.
	System.out.println("\nMuuttunut valikoima:");
	valikoima.tulostaTuotteet();
	
	//Luo tilaus
	Tilaus tilaus = new Tilaus();
	
	//Luo tilausrivit ja lisää tilausrivit tilaukseen
	//System.out.println("\nTilausrivien tulostus:");
    Tilausrivi tilausrivi;
    for (int tuotenro = 1; tuotenro <= tuotteita.length; tuotenro++) {
        tilausrivi = new Tilausrivi(valikoima, tuotenro, 1);
        if (tuotenro != poistettavaTuote) {
       // System.out.println(tilausrivi);
        tilaus.lisääRiviTilaukseen(tilausrivi);
        }
    }
    
    //Lisää tilaus kahvilan tilauslistaan.
    Kahvila kahvila = new Kahvila(valikoima);
	kahvila.lisääTilaus(tilaus);
	
	// Tulosta kaikki kahvilan tilaukset.
	System.out.println("\nTilaus:");
	kahvila.tulostaTilaukset();
    
}
}

