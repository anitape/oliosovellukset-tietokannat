package kahvila;

/**
 *
 * @author Anita Petunova 23.1.2020
 */
public class Testi {

    public static void main(String[] args) {

    // Esittele tarvittavat muuttujat.
    	int tuotenumero1, tuotenumero2, tuotenumero3;

    // Luo kolme tuotetta.
    	Tuote tuote1 = new Tuote("Croissant", 1.95);
    	Tuote tuote2 = new Tuote("Juustosämpylä", 3.50);
    	Tuote tuote3 = new Tuote("Ruisleipä", 3.00);

    // Lisää ne valikoimaan.
    	Valikoima valikoima = new Valikoima();
    	valikoima.lisääTuoteValikoimaan(tuote1);
    	valikoima.lisääTuoteValikoimaan(tuote2);
    	valikoima.lisääTuoteValikoimaan(tuote3);    	
    	
    // Tulosta valikoima.
    	//System.out.println(valikoima);
    	System.out.println("Tuotteet:");
    	valikoima.tulostaTuotteet();

    // Poista siitä yksi tuote.
    	tuotenumero1 = tuote1.getTuotenumero();
    	valikoima.poistaTuoteValikoimasta(tuotenumero1);
    	
    // Tulosta muuttunut valikoima.
    	System.out.println("\nMuuttunut valikoima:");
    	valikoima.tulostaTuotteet();
    	
    // Luo tilaus.
    	Tilaus tilaus1 = new Tilaus();
    	
    // Luo kaksi tilausriviä.
    	tuotenumero2 = tuote2.getTuotenumero();
    	tuotenumero3 = tuote3.getTuotenumero();
    	Tilausrivi rivi1 = new Tilausrivi(valikoima, tuotenumero2, 1);
    	Tilausrivi rivi2 = new Tilausrivi(valikoima, tuotenumero3, 1);

    // Lisää tilausrivit tilaukseen.
    	tilaus1.lisääRiviTilaukseen(rivi1);
    	tilaus1.lisääRiviTilaukseen(rivi2);
    	
    // Lisää tilaus kahvilan tilauslistaan.
    	Kahvila kahvila = new Kahvila(valikoima);
    	kahvila.lisääTilaus(tilaus1);

    // Luo toinenkin tilaus tilausriveineen.
    	//Tilaus tilaus2 = new Tilaus();
    	
    	
  	
    	
    // Tulosta kaikki kahvilan tilaukset.
    	System.out.println("\nTilaus:");
    	kahvila.tulostaTilaukset();
    }
}
