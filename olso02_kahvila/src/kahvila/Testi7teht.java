package kahvila;
import java.util.ArrayList;
import java.util.Scanner;
/**
*
* @author Anita Petunova 23.1.2020
*/
public class Testi7teht {

    
	public static void main(String[] args) {
		System.out.println("Tervetuloa Kahvilaan!");

		int toiminto;	
		int tuotenumero;
		String syotettyTuote;
		double syotettyHinta;
	    Valikoima valikoima = new Valikoima();
		Tilaus tilaus = new Tilaus();
	    Kahvila kahvila = new Kahvila(valikoima);
	    Tilausrivi tilausrivi;
		ArrayList<Tuote> tuotteita = new ArrayList<Tuote>();
		int syotettyPoistettavatuote;

		do {

			System.out.println("\nSyötä jokin seuraavista toiminnoista"
					+ "\n1. Lisää tuote valikoimaan\n2. Tulosta valikoima\n3. Poista tuote valikoimasta"
					+ "\n4. Kirjaa tilaus\n5. Tulostaa kaikki tilaukset\n0. Lopeta");
			Scanner numero = new Scanner(System.in);
			System.out.println("Anna toimintoa vastaava numero: ");
			toiminto = numero.nextInt();		
			// Esittele instanssimuuttujat

			if (toiminto == 1) {
					System.out.println("\n1. Lisää tuote valikoimaan");

						Scanner syotaTuote = new Scanner(System.in);
						System.out.println("Anna tuotteen nimi: ");
						syotettyTuote = syotaTuote.nextLine();
						
						Scanner syotaHinta = new Scanner(System.in);
						System.out.println("Anna tuotteen hinta: ");
						syotettyHinta = syotaHinta.nextDouble();
						
						tuotteita.add(new Tuote(syotettyTuote, syotettyHinta));					
			}
					
			else if (toiminto == 2) {
					for (Tuote tuote : tuotteita) {
						valikoima.lisääTuoteValikoimaan(tuote);
						System.out.println(tuote);
					}
						System.out.println("\n2. Tulosta valikoima");
						System.out.println("Tuotteet:");
						valikoima.tulostaTuotteet();
				}
					
			else if (toiminto == 3) {
					System.out.println("\n3. Poista tuote valikoimasta");
					Scanner poistettavaTuote = new Scanner(System.in);
					System.out.println("Anna poistettavan tuotteen tuotenumero: ");
					syotettyPoistettavatuote = poistettavaTuote.nextInt();
					
					System.out.println("\nPoistettavan tuotteen tuotenumero on: " + syotettyPoistettavatuote);
					valikoima.poistaTuoteValikoimasta(syotettyPoistettavatuote);
					
					System.out.println("\nMuuttunut valikoima:");
					valikoima.tulostaTuotteet();
			}
					
			else if (toiminto == 4) {
					System.out.println("\n4. Kirjaa tilaus");
					
					//Luo tilausrivit ja lisää tilausrivit tilaukseen
					//System.out.println("\nTilausrivien tulostus:");

				    for (int tuotenro = 1; tuotenro <= tuotteita.size(); tuotenro++) {
				        tilausrivi = new Tilausrivi(valikoima, tuotenro, 1);
				        tilaus.lisääRiviTilaukseen(tilausrivi);
				       /* if (tuotenro != syotettyPoistettavatuote) {
				       // System.out.println(tilausrivi);
				        
				        }*/
				    }
				    
				    //Lisää tilaus kahvilan tilauslistaan.				    
					kahvila.lisääTilaus(tilaus);
			}
					
			else if (toiminto == 5) {
					System.out.println("\n5. Tulostaa kaikki tilaukset ");
					System.out.println("\nTilaus:");
					kahvila.tulostaTilaukset();
			}
					
			else if (toiminto == 0) {
					System.out.println("/n0. Ohjelman lopetus");
				}
			
			else {
				/*System.out.println("\nSyötä jokin seuraavista toiminnoista"
						+ "\n1. Lisää tuote valikoimaan\n2. Tulosta valikoima\n3. Poista tuote valikoimasta"
						+ "\n4. Kirjaa tilaus\n5. Tulostaa kaikki tilaukset\n0. Lopeta");
				Scanner numero2 = new Scanner(System.in);
				System.out.println("Anna toimintoa vastaava numero: ");
				toiminto = numero2.nextInt();*/
			}
		} while(toiminto != 0);
	}
	}


