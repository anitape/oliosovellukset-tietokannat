package kahvila;
import java.util.ArrayList;
/**
 *
 * @author Anita Petunova 22.1.2020
 */
public class Tilaus {

    // Esittele ArrayList tilausrivien tallettamiseksi	
private ArrayList<Tilausrivi> tilausrivit = new ArrayList<Tilausrivi>();

    // Ohjelmoi lisääRiviTilaukseen()
public void lisääRiviTilaukseen(Tilausrivi tilausrivi) {
	tilausrivit.add(tilausrivi);
}
    // Ohjelmoi tulostaRivit()
public void tulostaRivit() {
	for (int i = 0; i < tilausrivit.size(); i++) {
		System.out.println(tilausrivit.get(i));
	}
}
    // Ohjelmoi annaLoppusumma()
public double annaLoppusumma() {
	double loppusumma = 0;
	for (int i = 0; i < tilausrivit.size(); i++) {
	Tilausrivi tilausrivi = tilausrivit.get(i);
	double hinta = tilausrivi.annaSumma();
	loppusumma = loppusumma + hinta;
	}
	
	return loppusumma;
	/*double hinta = tuote.getHinta();
	double loppusumma = hinta;
	return loppusumma;*/
}


}
