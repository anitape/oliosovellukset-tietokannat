package kahvila;

/**
 *
 * @author Anita Petunova 22.1.2020
 */
public class Tilausrivi {
	
    // Esittele instanssimuuttujat
    private Tuote tuote;
    private int kokomäärä;
    
    // Ohjelmoi kolmen parametrin konstruktori 
public Tilausrivi (Valikoima valikoima, int tuotenumero, int määrä) {
	tuote = valikoima.getTuote(tuotenumero);
	kokomäärä = määrä;
}
    // Ohjelmoi tulosta()
public void tulosta() {	
	System.out.println(tuote);
}
         
    // Ohjelmoi annaSumma()
public double annaSumma() {
	double hinta = tuote.getHinta();
	double summa = hinta * kokomäärä;
	return summa;
}

public String toString() {
	return kokomäärä + "kpl " + tuote;
}
}
