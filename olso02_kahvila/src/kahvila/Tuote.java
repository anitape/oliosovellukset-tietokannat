package kahvila;

/**
 *
 * @author Anita Petunova 22.1.2020
 */
public class Tuote {

    // Esittele instanssimuuttujat
private String nimi;
private double hinta;
private int tuotenumero;

    // Esittele staattinen muuttuja seuraavan vapaan tuotenumeron ylläpitämiseksi
private static int seuraavaVapaaTuotenumero = 1;
    // Ohjelmoi staattinen metodi alustaSeuraavaVapaaTuotenumero()
public static void alustaSeuraavaVapaaTuotenumero() {
	seuraavaVapaaTuotenumero = 1;
}
    // Ohjelmoi kahden parametrin konstruktori
public Tuote (String nimi, double hinta) {
	this.nimi = nimi;
	this.hinta = hinta;
	tuotenumero = seuraavaVapaaTuotenumero;
	seuraavaVapaaTuotenumero++;
}

    // Ohjelmoi getHinta()
public double getHinta() {
	return hinta;
}

    // Ohjelmoi getTuotenumero()
public int getTuotenumero() {
	return tuotenumero;
}
    // Ohjelmoi tosString()
public String toString() {
	return tuotenumero + " " + this.nimi + ", " + this.hinta + " €";
}
}
