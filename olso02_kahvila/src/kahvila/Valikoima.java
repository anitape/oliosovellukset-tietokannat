package kahvila;

import java.util.ArrayList;
/**
 *
 * @author Anita Petunova 22.1.2020
 */
public class Valikoima {

    // Esittele ArrayList tuotteiden tallettamiseksi
private ArrayList<Tuote> tuotteet = new ArrayList<Tuote>();

    // Ohjelmoi getTuote() - palauttaa tuotenumeroa vastaavan tuotteen nimen ja hinnan
public Tuote getTuote(int tuotenumero) {
	for (int i = 0; i < tuotteet.size(); i++) {
		Tuote tuote = tuotteet.get(i);
		if (tuotenumero == tuote.getTuotenumero()) {
			return tuote;   
		} 	
	}	
	return null;
}

    // Ohjelmoi lisääTuoteValikoimaan()
public void lisääTuoteValikoimaan(Tuote tuote) {
	tuotteet.add(tuote);
}

    // Ohjelmoi poistaTuoteValikoimasta()
    // Poistettava Tuote-olio pitää etsiä listasta tuotenumerolla,
    // koska poistojen jälkeen tuotenumero != listan indeksi!
public void poistaTuoteValikoimasta(int tuotenumero) {
	Tuote tuote = getTuote(tuotenumero);
	tuotteet.remove(tuote);
}

    // Ohjelmoi tulostaTuotteet()
public void tulostaTuotteet() {
	for (int i = 0; i<tuotteet.size(); i++) {
		System.out.println(tuotteet.get(i).toString());
	}
}
}
