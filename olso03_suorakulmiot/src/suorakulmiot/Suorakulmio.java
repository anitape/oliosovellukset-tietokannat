
package suorakulmiot;
/**
 *
 * @author Anita Petunova 20.1.2020 
 */ 
// Kirjoita tähän yleiskommentit
public class Suorakulmio implements Kuvio {
private int leveys, korkeus;

public void setLeveys(int leveys) {
	this.leveys = leveys;
}  

public void setKorkeus(int korkeus) {
	this.korkeus = korkeus;
}

public int getLeveys() {
	return leveys;
}

public int getKorkeus() {
	return korkeus;
}

public Suorakulmio (int leveys, int korkeus) {
	this.leveys = leveys;
	this.korkeus = korkeus;
}

public Suorakulmio() {}

public int ala() {
	return leveys*korkeus;
}

public String toString() {
	return "leveys=" + this.leveys + ", korkeus=" + this.korkeus;
	//return String.format("leveys=%d, korkeus=%d", leveys, korkeus);
}



}

