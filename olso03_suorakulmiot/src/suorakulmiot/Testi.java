
package suorakulmiot;
/**
 *
 * @author Anita Petunova 20.1.2020
 */

// Kirjoita tähän yleiskommentit

public class Testi {

    public static void main(String[] args) {

    // Kirjoita tähän testaamisessa tarvitsemasi lauseet
    NaytonSuorakulmio olio = new NaytonSuorakulmio(225, 120, 800, 30);
	System.out.println(olio.toString());
	olio.mahtuu(olio.getLeveys(), olio.getKorkeus());
	
	Suorakulmio kulmio = new NaytonSuorakulmio(225, 120,800,30);
	(NaytonSuorakulmio) kulmio.mahtuu(1024, 768);
    
	Suorakulmio olio2 = new Suorakulmio(800, 40);
	System.out.println(olio2.toString());
	System.out.println(olio2.ala());
    	
    }
}
