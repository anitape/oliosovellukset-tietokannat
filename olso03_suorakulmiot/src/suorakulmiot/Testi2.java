package suorakulmiot;
/**
*
* @author Anita Petunova 20.1.2020 
*/
public class Testi2 {

	public static void main(String[] args) {

	    // Kirjoita tähän testaamisessa tarvitsemasi lauseet
		System.out.println("testi2");
	    Kuvio naytonSuorakulmio = new NaytonSuorakulmio(225, 120, 800, 30);
		System.out.println(naytonSuorakulmio.toString());
		System.out.println((NaytonSuorakulmio) naytonSuorakulmio).mahtuu(1024,768)));
	    
		Suorakulmio olio2 = new Suorakulmio(80, 40);
		System.out.println(olio2.toString());
		System.out.println(olio2.ala());
}
}