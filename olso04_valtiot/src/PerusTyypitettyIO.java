/**
* @author Anita Petunova 27.1.2020
*/

import java.io.*;

public class PerusTyypitettyIO {	
	
	public static void kirjoita(int luvut[], String tiedosto) {
		FileOutputStream fileOut = null;		
		try {
			fileOut = new FileOutputStream(tiedosto);
		}
		catch(FileNotFoundException fe) {
			fe.printStackTrace();
		}
		
		DataOutputStream dataOut= new DataOutputStream(fileOut);
		for (int i = 0; i < luvut.length; i++) {
		try {
		dataOut.writeInt(luvut[i]);
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		}	
		//dataOut.close();
		// TODO: täydennä
	}

	public static int kerroKoko(String tiedosto) {
		FileInputStream f = null;
		//DataOutputStream d = new DataOutputStream(f);		
		try {
			f = new FileInputStream(tiedosto);
			return f.available();	
		}
		catch(IOException e) {
			return 0;
		} 
		// TODO: täydennä

		
	}

	public static byte[] annaTavuina(String tiedosto) {
		FileInputStream f=null;
		try {
			f = new FileInputStream(tiedosto);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}		
		byte[] tavut = new byte[kerroKoko(tiedosto)];
		try {
			//f = new FileInputStream(tiedosto);
			f.read(tavut);
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		
		// TODO: täydennä

		return tavut; // Palauta byte-taulukko
	}

	public static int[] annaKokonaislukuina(String tiedosto) {
		FileInputStream f = null;
		DataInputStream din = null;
		int[] joku = new int[kerroKoko(tiedosto)/4];
		try {
			f = new FileInputStream(tiedosto);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		din = new DataInputStream(f);
		for (int i = 0; i < joku.length; i++) {			
		try {

			joku[i] = din.readInt();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		}	
		// TODO: täydennä

		return joku; // Palauta int-taulukko
	}


	public static void main(String args[]) {
		String tiedosto = "Luvut.bin";
		int luvut[] = { 1, 200, 3, 4, 5 };
		byte tavut[];
		int iLuvut[];

		kirjoita(luvut, tiedosto);

		System.out.println("Tiedostossa on " + kerroKoko(tiedosto) + " tavua");

		System.out.println ("Tiedoston sisältö tavuittain:");
		tavut = annaTavuina(tiedosto);
		for (int i = 0; i < tavut.length; i++) {
			System.out.print(tavut[i] + " ");
		}

		System.out.println ("\nTiedoston sisältö kokonaislukuina:");
		iLuvut = annaKokonaislukuina(tiedosto);
		for (int i = 0; i < iLuvut.length; i++) {
			System.out.print(iLuvut[i] + " ");
		}
	}

}
