import java.util.Scanner;

/**
 * @author Anita Petunova 27.1.2020
 */

public class Poikkeus {

	public static void main(String[] args) {

		System.out.println("Tehtävä 1: Poikkeuksen testaaminen");

		// Esittele tarvittavat muuttujat

		String ika;
		
		// Kysy ikä ja lue se merkkijonomuuttujaan
		Scanner age = new Scanner(System.in);
		System.out.println("Anna ikäsi.");
		
		// Ohjelmoi try-catch-lohko
		while (true) {
			ika = age.nextLine();
			
		try {
			int afteryear = Integer.parseInt(ika) + 1;
			System.out.println("Vuoden päästä olet jo " + afteryear + "-vuotias.");
			break;
		}

		catch (NumberFormatException ne) {
			System.out.println("Virhe muuttujassa.");
		}
		}
		
		// Tee muunnos
		// - jos muunnos onnistuu, näytä tulos ja lopeta
		// - jos syntyy poikkeus, anna virheilmoitus

		// Poikkeustilanteessa ikää on kysyttävä vielä uudelleen,
		// joten tarvitset kaiken ympärille vielä toistorakenteen
	}
}
