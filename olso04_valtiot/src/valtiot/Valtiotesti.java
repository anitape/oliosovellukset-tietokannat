package valtiot;

/**
* @author Anita Petunova
*/

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Valtiotesti {

	public static void main(String[] args) {

		// Valtioiden käsittelyssä tarvittavat muuttujat ja
		// HashMap-olio Valtio-olioiden tallentamiseksi
		Valtio valtio;
		String nimi;
		String pääkaupunki;
		int asukasluku;
		HashMap<String, Valtio> valtiot = new HashMap<>();
		final String TIEDOSTONIMI = "valtiot.dat";
		
		
		Scanner näppis = new Scanner(System.in);
		//valtiot.get("Suomi");
		
		//Jos tiedosto on olemassa, lue HashMap sieltä 
		valtiot = TiedostonKäsittely.lueTiedosto(TIEDOSTONIMI);
		if (valtiot == null) {
			valtiot = new HashMap<>();
		}
		
		
		// Lisää valtioita HashMapiin, kunnes syötteenä annetaan tyhjä 

		do {
			System.out.println("Anna valtion nimi:");
			nimi = näppis.nextLine();
			if (nimi.length() == 0 ) {
				break;
			}
			
			System.out.println("Anna pääkaupungin nimi:");
			pääkaupunki = näppis.nextLine();
			
			System.out.println("Anna asukasluku:");
			asukasluku = Integer.parseInt(näppis.nextLine());
			
			valtiot.put(nimi, new Valtio(nimi, pääkaupunki, asukasluku));

		} while (nimi.length() != 0);
		
        // Tulosta HashMapin sisältö, käytä for-each- rakennetta
		System.out.println("HashMapissa nyt seuraavat valtiot:");
	    for (Map.Entry<String, Valtio> entry : valtiot.entrySet()) {	         
	        System.out.println(entry.getValue());
	      }

		// Kirjoita HashMap tiedostoon
		TiedostonKäsittely.kirjoitaTiedosto(TIEDOSTONIMI, valtiot);
    
		näppis.close();	
	}
	
	}

