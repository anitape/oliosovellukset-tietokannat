import saikeet.Juoksija;

/**
* @author Anita Petunova
*/

public class JuoksijaMain {
	public static void main(String[] args) throws InterruptedException {

	// Luo ja käynnistä Juoksija-säie
	Juoksija jsaie = new Juoksija();
	jsaie.start();
	// Odota jonkun aikaa: Thread.sleep()
	//jsaie.join();
	Thread.sleep(1000);
	// Pysäytä säie
	jsaie.lopeta();
	// Hae ja tulosta juostujen kerrosten määrä
	long kierrostenlkm = jsaie.getKierrokset();
	System.out.print(kierrostenlkm);
	}
}
