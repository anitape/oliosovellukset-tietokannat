
import saikeet.Kilpajuoksija;

/**
 * @author Anita Petunova
 */

public class KilpailuMain {

	public static void main(String[] args) throws InterruptedException {
		// Luo taulukko
		Kilpajuoksija[] juoksijat = new Kilpajuoksija[8];
		int voittaja = 0;
		int i;

		for (i = 0; i < juoksijat.length; i++) {
			juoksijat[i] = new Kilpajuoksija();
			juoksijat[i].start();			
			double valiaika = juoksijat[i].getAika();
			System.out.printf((i + 1) + ".kilpajuoksijan loppuaika on %.2f sec\n", valiaika);
		}
		
		for (i = 0; i < juoksijat.length; i++) {
		juoksijat[i].join();
		}
		
		System.out.println("Kilpailu on ohi, onnea voittajalle.");
	}
}
