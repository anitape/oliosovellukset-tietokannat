import saikeet.Kilpajuoksija2;
/**
 * @author Anita Petunova
 */
public class KilpailuMain2 {
	public static void main(String[] args) throws InterruptedException {
		// Luo taulukko
		Kilpajuoksija2[] juoksijat = new Kilpajuoksija2[8];
		int voittaja = 0;
		int i;

		for (i = 0; i < juoksijat.length; i++) {
			juoksijat[i] = new Kilpajuoksija2();
			juoksijat[i].start();
			juoksijat[i].join();
			double valiaika = juoksijat[i].getAika();
			System.out.printf((i + 1) + ".kilpajuoksijan loppuaika on %.2f sec\n", valiaika);
			juoksijat[i].wait(1000);
		}
		
		System.out.println("Kilpailu on ohi, onnea voittajalle.");
	}
}
