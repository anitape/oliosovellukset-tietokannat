import saikeet.Kilpajuoksija;

public class KilpajuoksijaMain {
	public static void main(String[] args) throws InterruptedException {

		// Luo ja käynnistä Juoksija-säie
		Kilpajuoksija kjsaie = new Kilpajuoksija();
		
		// Käynnistä säie
		kjsaie.start();
		
		// Odota säikeen päättymistä
		kjsaie.join();


		System.out.printf("Juoksijan loppuaika: %.3f sekunttia", kjsaie.getAika());
		}
}

// Vastaus tehtävään 6:
//Prioriteeteilla ei ole tässä tilanteessa mitään vaikutusta, koska loppuaika määräytyy Math.random()-metodin mukaan.
//Prioriteetit vaikuttaisivat pelkästään siihen,missä järjestyksessä kilpajuoksijoiden tiedot tulostuisivat ruudulle, mutta loppuaika
//säilyisi kuitenkin samana.