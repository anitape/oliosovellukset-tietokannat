import saikeet.LuuppaavaSaie;

/**
* @author Anita Petunova
*/

public class LuuppaavaSaieMain {
	public static void main(String[] args) throws InterruptedException {

    // Luo säie
	LuuppaavaSaie saie= new LuuppaavaSaie(20);
	
	// Käynnistä säie
	saie.start();
	// Odota säikeen päättymistä
	saie.join();
	}
}
