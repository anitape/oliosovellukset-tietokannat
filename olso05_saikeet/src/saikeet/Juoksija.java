package saikeet;

/**
* @author Anita Petunova
*/

public class Juoksija extends Thread {
	volatile boolean jatkuu = true;
	long kierrokset = 0;

	// Kirjoita kierrokset-muuttujan getteri
	public long getKierrokset() {
		return kierrokset;
	}
	
	
	@Override	
	public void run(){
		while (jatkuu) {
			kierrokset++;
			System.out.println(kierrokset);
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	}

	// Kun juokseminen halutaan lopettaa, joku toinen säie kutsuu 
	// lopeta()-metodia.
	public void lopeta(){
		 jatkuu = false;
	}
}
