package saikeet;
/**
 * @author Anita Petunova
 */

public class Kilpajuoksija extends Thread {
	private int numero; // juoksijan numero
	public static int seuraavaNumero = 1; // luokkamuuttuja juoksijoiden numeroimiseksi

	private final int MATKA = 400; // juostava matka, tässä vakio
	private double aika; // sekunteina
	// Konstruktori
	public Kilpajuoksija() {
		numero = seuraavaNumero;
		seuraavaNumero++;
	}

	@Override
	public void run(){

	// Ohjelmoi toisto, jossa
	// - käytät satunnaislukugenerattoria 10 metrin juoksuajan saamiseksi	
	// - odotat ko. ajan
	// - tulostat väliaikatieto 50 metrin välein	
		
	try {
		for (int i = 1; i <= MATKA/10; i++) {
		double randaika = Math.random() * (1.375-1.075) + 1.075;
		sleep((long) randaika);
		int juostumatka = i * 10;
		double valiaika = randaika *i;
		aika = valiaika;
		int jakojaannos = i%5;
		if (jakojaannos == 0) {
			// Tulostaa juoksijan numeron, juostu matka sekä väliaika 
			System.out.printf("Juoksijan numero: " + numero + " Juostu matka: " + juostumatka + " Aika:  %.2f sec\n", valiaika);
		}
	}		
	}catch (InterruptedException e) {
		e.printStackTrace();
	}
	}

	public double getAika() {
		return aika;
	}
}
