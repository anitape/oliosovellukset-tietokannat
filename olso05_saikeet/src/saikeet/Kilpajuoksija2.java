package saikeet;

/**
 * @author Anita Petunova
 */
public class Kilpajuoksija2 extends Thread {
	private int numero; // juoksijan numero
	public static int seuraavaNumero = 1; // luokkamuuttuja juoksijoiden numeroimiseksi

	private final int MATKA = 400; // juostava matka, tässä vakio
	private double aika; // sekunteina
	// Konstruktori

	public Kilpajuoksija2() {
		numero = seuraavaNumero;
		seuraavaNumero++;
	}

	@Override
	public void run() {

		// Ohjelmoi toisto, jossa
		// - käytät satunnaislukugenerattoria 10 metrin juoksuajan saamiseksi
		// - odotat ko. ajan
		// - tulostat väliaikatieto 50 metrin välein
		double time[] = new double[40];
		for (int i = 1; i <= MATKA / 10; i++) {
			double randaika = Math.random() * (1.375-1.075) + 1.075;
			double valiaika;
			time[i] = System.currentTimeMillis();
			if (i == 1) {
				valiaika = time[i];
				System.out.println(time[i]);
			}
			else { valiaika = time[i] - time[i - 1];
			System.out.println(time[i]);
			}
			
			aika = valiaika;
			int juostumatka = i * 10;
			int jakojaannos = i % 5;
			if (jakojaannos == 0) {
				// Tulostaa juoksijan numeron, juostu matka sekä väliaika
				System.out.printf(
						"Juoksijan numero: " + numero + " Juostu matka: " + juostumatka + " Aika:  %.2f sec\n",
						valiaika);
			}
		}

	}

	public double getAika() {
		return aika;
	}
}
