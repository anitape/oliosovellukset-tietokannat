package saikeet;

/**
 * @author Anita Petunova
 */

public class OmaSaie extends Thread {
	// Nämä tunnukset näkyvät saman pakkauksen luokkiin
	int luku;
	static int yhteisetAjokerrat;

	// Kirjoita parametriton konstruktori, joka ei tee mitään
	public OmaSaie() {

	}
	// Kirjoita parametrillinen konstruktori, joka asettaa arvon
	// instanssimuuttujalle luku
	public OmaSaie(int luku) {
		this.luku = luku;
	}
	
	// Kirjoita getterit ja setterit
	public void setLuku(int luku) {
		this.luku = luku;
	}
	
	public int getLuku() {
		return luku;
	}
	
	public static void setYhteisetAjokerrat(int yhteisetAjokerrat1) {
		yhteisetAjokerrat = yhteisetAjokerrat1;
	}
	
	public static int getYhteisetAjokerrat() {
		return yhteisetAjokerrat;
	}

	// Kirjoita metodi run(), joka kasvattaa kummankin muuttujan arvoa yhdellä
	public void run() {
		luku++;
		yhteisetAjokerrat++;
		
	}
}
