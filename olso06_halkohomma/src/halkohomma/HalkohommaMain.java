package halkohomma;

/**
 * @author Anita Petunova
 */

public class HalkohommaMain {
	public static void main(String args[]) throws InterruptedException {

		// Luo Halkovarasto
		Halkovarasto halkovarasto = new Halkovarasto(20);
		Halonhakkaaja[] hakkaajat = new Halonhakkaaja[6];
		Partiolainen[] partiolaiset = new Partiolainen[6];
		// Luo useita Halonhakkaaja-säikeitä

		for (int i = 0; i < 6; i++) {
			hakkaajat[i] = new Halonhakkaaja(halkovarasto);
			partiolaiset[i] = new Partiolainen(halkovarasto);
			hakkaajat[i].start();
			partiolaiset[i].start();
		}

	}
}