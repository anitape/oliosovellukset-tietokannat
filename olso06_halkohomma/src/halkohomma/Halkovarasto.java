package halkohomma;

/**
 * @author Anita Petunova
 */

public class Halkovarasto {
	private int max;
	private int halkojaVarastossa;

	public Halkovarasto(int max) {
		this.setMax(max); // vakiomäärä
		this.setHalkojaVarastossa(0);
	}

	public synchronized void lisaaHalkoja(int halko) throws InterruptedException {
		while (halkojaVarastossa < getMax()) {
			halkojaVarastossa += halko;
			if (halkojaVarastossa > getMax()) {
				halkojaVarastossa -= halko;
			}
			else {				
			System.out.println("Halonhakkaaja lisäsi " + halko + " halkoa. "
					+ "Varastossa on " + getHalkojaVarastossa() + " halkoa.");}
			wait();
		}
		notifyAll();
	}

	public synchronized void otaHalkoja(int halko) throws InterruptedException {
		while (halkojaVarastossa < halko) {
			wait();
		}
		notifyAll();
		halkojaVarastossa -= halko;
		System.out.println("Partiolainen otti " + halko + "halkoa. " +
		"Varastoon jäi " + getHalkojaVarastossa() + " halkoa.");

	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public int getHalkojaVarastossa() {
		return halkojaVarastossa;
	}

	public void setHalkojaVarastossa(int halkojaVarastossa) {
		this.halkojaVarastossa = halkojaVarastossa;
	}
}