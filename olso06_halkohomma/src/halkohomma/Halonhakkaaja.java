package halkohomma;

/**
* @author Anita Petunova
*/

public class Halonhakkaaja extends Thread {
	private Halkovarasto halkovarasto;
	private boolean valmis = false;
	
	public Halonhakkaaja(Halkovarasto halkovarasto) {
		this.halkovarasto = halkovarasto;
	}
	
	public void run() {
		while(!valmis) {
		int maara = (int) (Math.random()*4)+3;
		try {
			halkovarasto.lisaaHalkoja(maara);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	 
}
	public void lopeta() {
		valmis = true;
	}
}