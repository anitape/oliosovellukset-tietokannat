package halkohomma;

/**
* @author Anita Petunova
*/

public class Partiolainen extends Thread {
	private Halkovarasto halkovarasto;
	private boolean valmis = false;
	
	public Partiolainen(Halkovarasto halkovarasto) {
		this.halkovarasto = halkovarasto;
	}
	
	public void run() {
	 while(!valmis) {
		 int halko = (int) (Math.random()*4)+3;
		 try {
			 halkovarasto.otaHalkoja(halko);
			 Thread.sleep(1000);
		 }catch (InterruptedException e) {
			 e.printStackTrace();
		 }
	 }
}
	public void lopeta() {
		valmis = true;
	}
}