package sharedXY;

/**
* @author Anita Petunova
*/

public class SharedXY {
	private int x;
	private int y;
	private int w;

	// Kirjoita konstruktori, joka asettaa muuttujille alkuarvot
	public SharedXY(int x, int y) {
		this.x = x;
		this.y = y;
	}
	// Kirjoita metodi swap(), joka vaihtaa arvot keskenään
	 public synchronized void swap() {
		 w = y;
		 y = x;
		 x = w;
		 System.out.println("Vaihdon jälkeen\nx = " + x + "\ny = " + y);
	 }
	 
	 public synchronized boolean xsametoy() {
		 return x == y;
	 }
	 
	 /*public int getX() {
		 return x;
	 }
	 
	 public int getY() {
		 return y;
	 }*/
}