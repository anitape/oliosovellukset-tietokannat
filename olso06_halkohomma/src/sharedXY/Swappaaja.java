package sharedXY;

/**
* @author Anita Petunova
*/

public class Swappaaja extends Thread{
	SharedXY sharedxy;
	// Kirjoita konstruktori
	 public Swappaaja (SharedXY sharedxy) {
		 this.sharedxy = sharedxy;
	 }
	
	
	// Kirjoita metodi run()
	public void run() {	
		while (sharedxy.xsametoy() == false) {
			sharedxy.swap();
		}
		System.exit(0);
	}
}