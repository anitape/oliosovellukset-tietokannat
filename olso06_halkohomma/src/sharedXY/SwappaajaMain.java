package sharedXY;

/**
* @author Anita Petunova
*/

public class SwappaajaMain {
	 public static void main(String args[]) {
		// Luo yhteiskäyttöinen tietorakenneolio
		SharedXY sharedxy = new SharedXY(5,7);
		
		// Luo useita Swappaaja-säikeitä
		Swappaaja swappaaja1 = new Swappaaja(sharedxy);
		Swappaaja swappaaja2 = new Swappaaja(sharedxy);
		Swappaaja swappaaja3 = new Swappaaja(sharedxy);
		Swappaaja swappaaja4 = new Swappaaja(sharedxy);
		
		swappaaja1.start();
		swappaaja2.start();
		swappaaja3.start();
		swappaaja4.start();		
	 }
}