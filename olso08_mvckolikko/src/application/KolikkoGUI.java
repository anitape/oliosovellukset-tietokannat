package application;

import java.io.FileInputStream;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class KolikkoGUI extends Application {
	
private final controller.kontrolleri control = new controller.kontrolleri(this,2);
	Text txt;
	public void start(Stage primaryStage) {
		
		try {
			primaryStage.setTitle("Kolikon heitto");
			FileInputStream kruunu = control.palautaKruunu();
			FileInputStream klaava = control.palautaKlaava();
			Image image1 = new Image(kruunu);
			Image image2 = new Image(klaava);
			ImageView iv1 = new ImageView(image1);
			BorderPane root = new BorderPane(iv1);
			Button btn = new Button("Heitä kolikko");
			
			btn.setOnAction(new EventHandler<ActionEvent>() {
		@Override
		public void handle(ActionEvent event) {
			int maara = control.annaMaarakolikko();
			int luku = control.annaKruunuklaava();
			if (luku == 1) {
				iv1.setFitWidth(100);
				iv1.setFitHeight(100);
			    iv1.setImage(image2);
			txt = new Text("Klaava " + "\nHeittojen lkm: " + maara);
			}
			else {
				iv1.setFitWidth(100);
				iv1.setFitHeight(100);
			    iv1.setImage(image1);
			txt = new Text("Kruunu " + "\nHeittojen lkm: " + maara);	
			}
			txt.setFont(Font.font("Arial", FontWeight.BOLD, 20));
			BorderPane.setAlignment(txt, Pos.CENTER);
			BorderPane.setAlignment(iv1, Pos.CENTER);
			root.setBottom(txt);
			root.setTop(iv1);
		}
			});
			root.setCenter(btn);
			primaryStage.setScene(new Scene(root, 600, 300));
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}
}
