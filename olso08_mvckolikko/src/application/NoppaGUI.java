package application;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import controller.kontrolleri;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class NoppaGUI extends Application{
	
	private final kontrolleri control = new kontrolleri(this,6);
	public void start(Stage primaryStage) throws FileNotFoundException {
		
		primaryStage.setTitle("Nopan heitto");		
		FileInputStream palautettu1 = control.palautaKuva1();
		FileInputStream palautettu2 = control.palautaKuva2();
		FileInputStream palautettu3 = control.palautaKuva3();
		FileInputStream palautettu4 = control.palautaKuva4();
		FileInputStream palautettu5 = control.palautaKuva5();
		FileInputStream palautettu6 = control.palautaKuva6();
		Image image1 = new Image(palautettu1);
		Image image2 = new Image(palautettu2);		
		Image image3 = new Image(palautettu3);
		Image image4 = new Image(palautettu4);
		Image image5 = new Image(palautettu5);
		Image image6 = new Image(palautettu6);		
		ImageView iv1 = new ImageView(image1);
	    BorderPane root = new BorderPane(iv1);
		Button btn = new Button("Heitä noppa");
		btn.setOnAction(new EventHandler<ActionEvent>() {				
				
		@Override
		public void handle(ActionEvent event) {	
			int maara = control.annaMaara();
			int luku = control.annaLuku();
			if (luku == 1) {
				iv1.setFitWidth(50);
				iv1.setFitHeight(50);
			    iv1.setImage(image1);
			}
			
			else if (luku == 2) {
				iv1.setFitWidth(50);
				iv1.setFitHeight(50);
			    iv1.setImage(image2);
			}
			
			else if (luku == 3) {
				iv1.setFitWidth(50);
				iv1.setFitHeight(50);
			    iv1.setImage(image3);
			}
			
			else if (luku == 4) {
				iv1.setFitWidth(50);
				iv1.setFitHeight(50);
			    iv1.setImage(image4);
			}
			
			else if (luku == 5) {
				iv1.setFitWidth(50);
				iv1.setFitHeight(50);
			    iv1.setImage(image5);
			}
			
			else {
				iv1.setFitWidth(50);
				iv1.setFitHeight(50);
			    iv1.setImage(image6);
			}
			Text txt = new Text("Tulos: " + luku + ". Heittokertoja: " + maara + ".");
			/*String teksti = control.heita5noppaa();
			Text txt = new Text(teksti);*/
			txt.setFont(Font.font("Arial", FontWeight.BOLD, 20));
			BorderPane.setAlignment(txt, Pos.CENTER);
			BorderPane.setAlignment(iv1, Pos.CENTER);
			root.setBottom(txt);
			root.setTop(iv1);
		}
			});
			root.setCenter(btn);
			primaryStage.setScene(new Scene(root, 600, 300));
			primaryStage.show();
			
	}


	public static void main(String[] args) {
		launch(args);
	}
}

