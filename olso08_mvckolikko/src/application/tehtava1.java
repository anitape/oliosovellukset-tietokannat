package application;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class tehtava1 extends Application {

	public void start(Stage primaryStage) {
		try {
			primaryStage.setTitle("Tervehdyksiä");
			BorderPane root = new BorderPane();
			Button btn1 = new Button("Aamu");
			Button btn2 = new Button("Päivä");
			Button btn3 = new Button("Ilta");
			Button btn4 = new Button("Yö");
			Text txt = new Text("Napsauta painiketta.");
			txt.setFont(Font.font("Arial", FontWeight.BOLD, 20));
			BorderPane.setAlignment(txt, Pos.CENTER);
			root.setTop(txt);
			btn1.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					Text txt = new Text("Hyvää huomenta");
					txt.setFont(Font.font("Arial", FontWeight.BOLD, 20));
					BorderPane.setAlignment(txt, Pos.CENTER);
					root.setTop(txt);
				}
			});

			btn2.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					Text txt = new Text("Hyvää päivää!");
					txt.setFont(Font.font("Arial", FontWeight.BOLD, 20));
					BorderPane.setAlignment(txt, Pos.CENTER);
					root.setTop(txt);
				}
			});

			btn3.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					Text txt = new Text("Hyvää iltaa!");
					txt.setFont(Font.font("Arial", FontWeight.BOLD, 20));
					BorderPane.setAlignment(txt, Pos.CENTER);
					root.setTop(txt);
				}
			});

			btn4.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					Text txt = new Text("Hyvää yötä!");
					txt.setFont(Font.font("Arial", FontWeight.BOLD, 20));
					BorderPane.setAlignment(txt, Pos.CENTER);
					root.setTop(txt);
				}
			});

			root.setLeft(btn1);
			root.setCenter(btn2);
			root.setRight(btn3);
			root.setBottom(btn4);
			primaryStage.setScene(new Scene(root, 300, 150));
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	public static void main(String[] args) {
		launch(args);
	}
}
