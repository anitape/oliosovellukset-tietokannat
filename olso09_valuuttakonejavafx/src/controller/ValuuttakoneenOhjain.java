package controller;

import model.IValuuttakone;
import model.Valuuttakone;
import view.IValuuttakoneenGUI;
import view.ValuuttaKoneenGUI;

public class ValuuttakoneenOhjain implements IValuuttakoneenOhjain {

	private Valuuttakone vk;
	private ValuuttaKoneenGUI gui;
	
	public ValuuttakoneenOhjain(ValuuttaKoneenGUI gui){
		this.vk = new Valuuttakone();
		this.gui = gui; 
	}
	
	@Override
	public void muunnos() {
		// TODO Auto-generated method stub
		gui.setTulos(vk.muunna(gui.getLähtöIndeksi(), gui.getKohdeIndeksi(), gui.getMäärä()));
	}

	@Override
	public String[] getValuutat() {
		return vk.getVaihtoehdot();
	}
	}


/*
IPainoindeksiModel model = new PainoindeksiModel();
IPainoindeksiGUI gui;

public PainoindeksiKontrolleri(IPainoindeksiGUI gui) {
	this.gui = gui;
}

public void painoindeksinLaskenta(){	
		gui.setIndeksi(model.laskeIndeksi(gui.getPituus(), gui.getPaino()));
	}*/