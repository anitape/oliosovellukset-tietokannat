package model;

public class Valuuttakone implements IValuuttakone {

	Valuutta[] valuutat;
	Valuutta valuutta;
	
	public Valuuttakone() {
		valuutat=new Valuutta[] {
		new Valuutta("Euroopan euro", 1.0),
		new Valuutta("Ruotsin kruunu", 9.4780),
		new Valuutta("Norjan kruunu", 8.9018),
		new Valuutta("Yhdysvaltain dollari", 1.0629)
	};
	}
	
	@Override
	public String[] getVaihtoehdot() {
		// TODO Auto-generated method stub
		String[] palautus = new String[valuutat.length];		
		for (int i = 0; i < valuutat.length; i++) {
		palautus[i] = valuutat[i].getNimi();
	}
		return palautus;
	}

	@Override
	public double muunna(int mistäIndeksi, int mihinIndeksi, double määrä) {
		// TODO Auto-generated method stub
		return (määrä/valuutat[mistäIndeksi].getVaihtokurssi())*(valuutat[mihinIndeksi].getVaihtokurssi());
		/*double tulos = 0;
		if (mistäIndeksi == 0) {
			if (mihinIndeksi == 0) {
				tulos = 1.0 * määrä;
			}

			else if (mihinIndeksi == 1) {
				tulos = 9.4780 * määrä;
			}

			else if (mihinIndeksi == 2) {
				tulos = 8.9018 * määrä;
			}

			else if (mihinIndeksi == 3){
				tulos = 1.0629 * määrä;
			}
		}

		else if (mistäIndeksi == 1) {
			double valitulos = määrä / 9.4780;
			if (mihinIndeksi == 0) {
				tulos = valitulos;
			}
			
			else if (mihinIndeksi == 1) {
				tulos = 1.0 * määrä;
			}

			else if (mihinIndeksi == 2) {
				tulos = valitulos * 8.9018;
			}

			else if (mihinIndeksi == 3) {
				tulos = valitulos * 1.0629;
			}
		}
		
		else if (mistäIndeksi == 2) {
			double valitulos = määrä / 8.9018;
			if (mihinIndeksi == 0) {
				tulos = valitulos;
			}
			
			else if (mihinIndeksi == 1) {
				tulos = valitulos * 9.4780;
			}

			else if (mihinIndeksi == 2) {
				tulos = 1.0 * määrä;
			}

			else if (mihinIndeksi == 3) {
				tulos = valitulos * 1.0629;
			}
		}
		
		else if (mistäIndeksi == 3) {
			double valitulos = määrä / 1.0629;
			if (mihinIndeksi == 0) {
				tulos = valitulos;
			}
			
			else if (mihinIndeksi == 1) {
				tulos = valitulos * 9.4780;
			}

			else if (mihinIndeksi == 2) {
				tulos = valitulos * 8.9018;
			}

			else if (mihinIndeksi == 3) {
				tulos = 1.0 * määrä;
			}
		}*/
		
	}


}
