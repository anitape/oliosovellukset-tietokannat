package model;

import java.sql.*;
import java.util.ArrayList;

public class ValuuttaAccessObject implements IValuuttaDAO {

	private Connection myCon;

	public ValuuttaAccessObject() { // konstruktori
		try {
			Class.forName("com.mysql.jdbc.Driver");
			myCon = DriverManager.getConnection("jdbc:mysql://localhost/valuutta", "root", "anita910225");
		} catch (Exception e) {
			System.err.println("Virhe tietokantayhteyden muodostamisessa.");
			System.exit(-1); // turha jatkaa...
		}
	}

	@Override
	public void finalize() { // destruktori
		try { // oli sama yhteys koko sovelluksen ajan
			if (myCon != null)
				myCon.close(); // vapauttaa muutkin resurssit
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public Valuutta readValuutta(String tunnus) {
		Valuutta valuutta = null;
		try (PreparedStatement myStatement = myCon.prepareStatement("SELECT * FROM valuutta WHERE tunnus=?")) {
			myStatement.setString(1, tunnus);
			// suorita kysely ja ota vastaan tulos
			ResultSet myRs = myStatement.executeQuery();
			if (myRs.next()) { // vain yksi voi löytyä (yksikäsitteinen avain)
				String rs_tunnus = myRs.getString("tunnus");
				double rs_vaihtokurssi = myRs.getDouble("vaihtokurssi");
				String rs_nimi = myRs.getString("nimi");
				valuutta = new Valuutta(); // luo DTO ja vie saraketiedot ominaisuuskenttiin
				valuutta.setTunnus(rs_tunnus);
				valuutta.setVaihtokurssi(rs_vaihtokurssi);
				valuutta.setNimi(rs_nimi);
			}

		} catch (SQLException e) {
			do {
				System.err.println("Viesti: " + e.getMessage());
				System.err.println("Virhekoodi: " + e.getErrorCode());
				System.err.println("SQL-tilakoodi: " + e.getSQLState());
			} while (e.getNextException() != null);
		}
		return valuutta; // joko null tai tietokannasta haetut tiedot Valuutta-oliona
	}
	
	@Override
	public Valuutta[] readValuutat() { // Palauttaa taulukollisen Car-olioita
		ArrayList<Valuutta> list = new ArrayList();
		try (PreparedStatement myStatement = myCon.prepareStatement("SELECT * FROM valuutta")) {
			ResultSet myRs = myStatement.executeQuery();
			while (myRs.next()) { // nyt tulosjoukossa voi olla useita rivejä
				Valuutta valuutta = new Valuutta(); // luo DTO ja vie saraketiedot ominaisuuskenttiin
				valuutta.setTunnus(myRs.getString("tunnus"));
				valuutta.setVaihtokurssi(myRs.getDouble("vaihtokurssi"));
				valuutta.setNimi(myRs.getString("nimi"));
				list.add(valuutta); // lisää listaan
			}
		} catch (SQLException e) {
			do {
				System.err.println("Viesti: " + e.getMessage());
				System.err.println("Virhekoodi: " + e.getErrorCode());
				System.err.println("SQL-tilakoodi: " + e.getSQLState());
			} while (e.getNextException() != null);
		}
		Valuutta[] returnArray = new Valuutta[list.size()];
		return (Valuutta[]) list.toArray(returnArray);
	}
	
	@Override
	public boolean createValuutta(Valuutta valuutta) {
		try (PreparedStatement myStatement = myCon.prepareStatement("INSERT INTO valuutta VALUES (?, ?, ?)")) {
			myStatement.setString(1, valuutta.getTunnus());
			myStatement.setDouble(2, valuutta.getVaihtokurssi());
			myStatement.setString(3, valuutta.getNimi());
			myStatement.executeUpdate();
			return true;
		}

		catch (SQLException e) {
			// TODO Auto-generated catch block
			do {
				System.err.println("Viesti: " + e.getMessage());
				System.err.println("Virhekoodi: " + e.getErrorCode());
				System.err.println("SQL-tilakoodi: " + e.getSQLState());
			} while (e.getNextException() != null);
			return false;
		}
	}
	
	@Override
	public boolean updateValuutta(Valuutta valuutta) {
		try (PreparedStatement myStatement = myCon
				.prepareStatement("UPDATE valuutta SET nimi= ?, vaihtokurssi= ? WHERE tunnus=?")) {
			myStatement.setString(3, valuutta.getTunnus());
			myStatement.setDouble(2, valuutta.getVaihtokurssi());
			myStatement.setString(1, valuutta.getNimi());
			myStatement.executeUpdate();
			// suorita kysely ja ota vastaan tulos
			
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			do {
				System.err.println("Viesti: " + e.getMessage());
				System.err.println("Virhekoodi: " + e.getErrorCode());
				System.err.println("SQL-tilakoodi: " + e.getSQLState());
			} while (e.getNextException() != null);
			return false;
		}
	}
	
	@Override
	public boolean deleteValuutta(String tunnus) {
		try 
		(PreparedStatement myStatement = myCon.prepareStatement("DELETE FROM valuutta WHERE tunnus=?")){
			myStatement.setString(1, tunnus);
			//myStatement.executeUpdate();
			if (myStatement.executeUpdate()==0) {
				return false;
			}
			
			return true;
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			do {
				System.err.println("Viesti: " + e.getMessage());
				System.err.println("Virhekoodi: " + e.getErrorCode());
				System.err.println("SQL-tilakoodi: " + e.getSQLState());
			} while (e.getNextException() != null);
			return false;
		}
	}

}
