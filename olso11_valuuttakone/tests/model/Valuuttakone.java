package model;

public class Valuuttakone implements IValuuttakone {

	Valuutta[] valuutat;
	Valuutta valuutta;
	ValuuttaAccessObject vao;
	
	public Valuuttakone() {
		vao = new ValuuttaAccessObject();
		valuutat=vao.readValuutat();
	};
	
	
	@Override
	public String[] getVaihtoehdot() {
		// TODO Auto-generated method stub
		String[] palautus = new String[valuutat.length];		
		for (int i = 0; i < valuutat.length; i++) {
		palautus[i] = valuutat[i].getNimi();
	}
		return palautus;
	}

	@Override
	public double muunna(int mistäIndeksi, int mihinIndeksi, double määrä) {
		// TODO Auto-generated method stub
		return (määrä/valuutat[mistäIndeksi].getVaihtokurssi())*(valuutat[mihinIndeksi].getVaihtokurssi());
		
		
	}


}
