package main;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;




import model.Valuutta;

public class Main {

	public static void main(String[] args) {
		Valuutta valuutta = new Valuutta();
		
		valuutta.setTunnus("SEK");
		valuutta.setVaihtokurssi(9.45);
		valuutta.setNimi("Ruotsin kruunu");
		
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		session.save(valuutta);
		session.getTransaction().commit();
		session.close();
		sessionFactory.close();
	}
}
