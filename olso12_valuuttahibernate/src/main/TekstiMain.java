package main;

import java.util.Scanner;
import model.*;

public class TekstiMain {
	static ValuuttaAccessObject valuuttaDAO = new ValuuttaAccessObject();
	static Scanner scanner = new Scanner(System.in);

	public static void listaaValuutat() {
		valuuttaDAO.readValuutat();
		/*Valuutta[] vlista = valuuttaDAO.readValuutat();
		for (int i = 0; i < vlista.length; i++) {
			System.out.println("Lista");
			System.out.println(vlista[i]);
		}*/
	}
	
	public static void lisääValuutta() {
		String[] parameter = new String[3];
		String[] tekstit = new String[] {"Anna tunnus: ", "Anna vaihtokurssi: ", "Anna nimi: "};		
		for (int i = 0; i < 3; i++) {
			System.out.println(tekstit[i]);
			parameter[i] = scanner.nextLine();
		}
		valuuttaDAO.createValuutta(new Valuutta(parameter[0], Double.parseDouble(parameter[1]), parameter[2]));
	}

	public static void päivitäValuutta() {
		String[] parameter = new String[3];
		String[] tekstit = new String[] {"Anna tunnus: ", "Anna vaihtokurssi: ", "Anna nimi: "};
		for (int i = 0; i < 3; i++) {
			System.out.println(tekstit[i]);
			parameter[i] = scanner.nextLine();
		}
		valuuttaDAO.updateValuutta(new Valuutta(parameter[0], Double.parseDouble(parameter[1]), parameter[2]));
	}

	public static void poistaValuutta() {
		System.out.println("Anna tunnus: ");
		valuuttaDAO.deleteValuutta(scanner.nextLine());
	}

	public static void main(String[] args) {
		char valinta;
		final char CREATE = 'C', READ = 'R', UPDATE = 'U', DELETE = 'D', QUIT = 'Q';

		do {
			System.out.println(
					"C: Lisää uusi valuutta tietokantaan\n" + "R: Listaa tietokannassa olevien valuuttojen tiedot\n"
							+ "U: Päivitä valuutan vaihtokurssi tietokantaan\n" + "D: Poista valuutta tietokannasta\n"
							+ "Q. Lopetus");
			System.out.println("Valintasi:_ ");

			valinta = (scanner.nextLine().toUpperCase()).charAt(0);
			switch (valinta) {
			case CREATE:
				lisääValuutta();
				break;
			case READ:
				listaaValuutat();
				break;
			case UPDATE:
				päivitäValuutta();
				break;
			case DELETE:
				poistaValuutta();
				break;
			}
		} while (valinta != QUIT);
	}
}
