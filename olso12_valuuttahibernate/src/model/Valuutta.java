package model;

import javax.persistence.*;
/**
 * @author Oma nimesi
 */
@Entity
@Table(name="valuutta") 
public class Valuutta {
	
	@Id
	@Column(name ="tunnus")
	private String tunnus;
	
	@Column(name ="vaihtokurssi")
	private double vaihtokurssi;	
	
	@Column(name ="nimi")
	private String nimi;
	
	public Valuutta(String tunnus, double vaihtokurssi, String nimi) {
		this.setTunnus(tunnus);
		this.setVaihtokurssi(vaihtokurssi);
		this.setNimi(nimi);		
	}
	
	public Valuutta() {
		
	}

	public String getTunnus() {
		return tunnus;
	}

	public void setTunnus(String tunnus) {
		this.tunnus = tunnus;
	}

	public double getVaihtokurssi() {
		return vaihtokurssi;
	}

	public void setVaihtokurssi(double vaihtokurssi) {
		this.vaihtokurssi = vaihtokurssi;
	}

	public String getNimi() {
		return nimi;
	}

	public void setNimi(String nimi) {
		this.nimi = nimi;
	}
	
	public String toString() {
		return tunnus+" "+vaihtokurssi+" "+nimi;
	}
	
	
}
