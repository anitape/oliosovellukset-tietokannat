package model;

import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class ValuuttaAccessObject implements IValuuttaDAO {

	private SessionFactory istuntotehdas = null;

	public ValuuttaAccessObject() { // konstruktori
		try {
			istuntotehdas = new Configuration().configure().buildSessionFactory();
		} catch (Exception e) {
			System.err.println("Istuntotehtaan luonti ei onnistunut: " + e.getMessage());
			System.exit(-1);
		}
	}

	@Override
	public void finalize() { // destruktori
		try {
			if (istuntotehdas != null)
				istuntotehdas.close(); // sulkee automaattisesti muutkin avatut resurssit
		} catch (Exception e) {
			System.err.println("Istuntotehtaan sulkeminen epäonnistui: " + e.getMessage());
		}
	}

	@Override
	public Valuutta readValuutta(String tunnus) {
		Session istunto = istuntotehdas.openSession();
		istunto.beginTransaction();
		// Tietokantaa muuttavat operaatiot tänne
		Valuutta valuutta = new Valuutta(); // Tuo tiedot olemassaolevaan olioon
		istunto.load(valuutta, tunnus); // Jos riviä ei ole DB:ssä, heittää ObjectNotFoundException
		System.out.println(valuutta.getTunnus());
		istunto.getTransaction().commit();
		istunto.close();
		return valuutta;
	}

	@SuppressWarnings("unchecked")
	public Valuutta[] readValuutat() { // Palauttaa taulukollisen Valuutta-olioita
		Session istunto = istuntotehdas.openSession();
		List<Valuutta> result = istunto.createQuery("from Valuutta").getResultList();
		for (Valuutta v : result) {
			System.out.println(v.getTunnus() + " " + v.getVaihtokurssi() + " " + v.getNimi());
		}
		istunto.close();
		Valuutta[] returnArray = new Valuutta[result.size()];
		return (Valuutta[]) result.toArray(returnArray);
	}

	@Override
	public boolean createValuutta(Valuutta valuutta) {
		Transaction transaktio = null;
		try (Session istunto = istuntotehdas.openSession()) {
			transaktio = istunto.beginTransaction();
			// Tietokantaa muuttavat operaatiot tänne
			istunto.saveOrUpdate(valuutta);
			istunto.getTransaction().commit();			
			// istunto.close();
			return true;
		} catch (Exception e) {
			if (transaktio != null)
				transaktio.rollback();
			return false;
		}

	}

	@Override
	public boolean updateValuutta(Valuutta valuutta) {
		Transaction transaktio = null;
		try (Session istunto = istuntotehdas.openSession()) {
			transaktio = istunto.beginTransaction();
			istunto.saveOrUpdate(valuutta);
			istunto.getTransaction().commit();
			// istunto.close();
			return true;
		} catch (Exception e) {
			if (transaktio != null)
				transaktio.rollback();
			return false;
		}
		
	}

	@Override
	public boolean deleteValuutta(String tunnus) throws NoResultException {
		Transaction transaktio = null;
		try (Session istunto = istuntotehdas.openSession()) {
			transaktio = istunto.beginTransaction();
			Valuutta valuutta = new Valuutta();
			istunto.load(valuutta, tunnus);
			istunto.delete(valuutta);
			istunto.getTransaction().commit();			
			// istunto.close();
			return true;
		} catch (Exception e) {
			if (transaktio != null)
				transaktio.rollback();
			return false;
		}
		
	}

}
