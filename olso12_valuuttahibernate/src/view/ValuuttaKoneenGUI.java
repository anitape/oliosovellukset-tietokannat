package view;

import java.io.FileNotFoundException;
import java.text.DecimalFormat;

import controller.IValuuttakoneenOhjain;
import controller.ValuuttakoneenOhjain;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.Valuuttakone;

public class ValuuttaKoneenGUI extends Application implements IValuuttakoneenGUI {

	private ValuuttakoneenOhjain kontrolleriv;
	private Valuuttakone vkone = new Valuuttakone();
	private ListView<String> rahalista;
	private ListView<String> rahalista2;
	private TextField tulosField;
	private TextField maaraField;
	
	public void init() {
		// MVC-rakenteen kuuluvan kontrollerin luonti
		kontrolleriv = new ValuuttakoneenOhjain(this);
	}
	@Override
	public void start(Stage primaryStage) throws FileNotFoundException {
		// Käyttöliittymän rakentaminen
		primaryStage.setTitle("Valuuttakone");
		
		HBox root = createVBoxs();
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.show();
	}	
	
	private HBox createVBoxs() {
		// Perusrakenteena horisontaalinen boxi

        HBox hbox = new HBox();
        hbox.setPadding(new Insets(15, 12, 15, 12)); // marginaalit ylä, oikea, ala, vasen
        hbox.setSpacing(10);   // noodien välimatka 10 pikseliä
        //hbox.setStyle("-fx-background-color: #336699;");
		
        //EKA PYSTYSUORA BOXI
        VBox vbox1 = new VBox();
       // vbox1.setPadding(new Insets(15, 12, 15, 12));
       // vbox1.setSpacing(10);

        // Tuotteet (kaksi ListView-kontrollia: vaatteille ja väreille):
        rahalista = new ListView<String>();
        rahalista.setPrefSize(150, 130);
        ObservableList<String> rahat = FXCollections.observableArrayList ();
        rahat.addAll(kontrolleriv.getValuutat());
        rahalista.setItems(rahat);

        // laita ListView VBoxiin, niin saadaan otsikko yl�puolelle
        vbox1.getChildren().addAll(new Label("Mistä"), rahalista);

        //TOINEN PYSTYSUORA BOXI
        VBox vbox2 = new VBox();
        rahalista2 = new ListView<String>();
        rahalista2.setPrefSize(150, 130);
        ObservableList<String> rahat2 =FXCollections.observableArrayList ();
        rahat2.addAll(kontrolleriv.getValuutat());
        rahalista2.setItems(rahat2);

        // laita ListView VBoxiin , niin saadaan otsikko yl�puolelle
        vbox2.getChildren().addAll(new Label("Mihin"), rahalista2);
              
       //KOLMAS PYSTYSUORA BOXI
        VBox vbox3 = new VBox();
        Label maara = new Label("Määrä");
        maaraField = new TextField();
        Button muunna = new Button("Muunna");
        Label tulos = new Label("Tulos");
        tulosField = new TextField();
        Text summa = new Text();
        vbox3.getChildren().addAll(maara, maaraField, muunna, tulos, tulosField);       
        muunna.setPrefSize(100, 20);
        
        
        muunna.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
        		DecimalFormat df = new DecimalFormat();
        		df.setMaximumFractionDigits(2);
                tulosField.setText(""+df.format(vkone.muunna(getLähtöIndeksi(), getKohdeIndeksi(), getMäärä())));
            }
        });

        // Täytä pääboxi:
        hbox.getChildren().addAll(vbox1, vbox2, vbox3);

        return hbox;
    }
	
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public int getLähtöIndeksi() {
		// TODO Auto-generated method stub
		return rahalista.getSelectionModel().getSelectedIndex();
	}

	@Override
	public int getKohdeIndeksi() {
		// TODO Auto-generated method stub
		return rahalista2.getSelectionModel().getSelectedIndex();
	}

	@Override
	public double getMäärä() {
		// TODO Auto-generated method stub
		double vastaus = 0;
		try {			
		vastaus = Double.parseDouble(maaraField.getText());
		}
		catch (NumberFormatException e) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Huomio!");
			alert.setHeaderText(null);
			alert.setContentText("Käytä numeroita!");

			alert.showAndWait();
		}
		return vastaus;
	}

	@Override
	public void setTulos(double määrä) {
		// TODO Auto-generated method stub
		//double maara = Double.parseDouble(maaraField.getText());
		tulosField.setText("" +määrä);
	}
}

/*
 * @Override public void init(){ // MVC-rakenteen kuuluvan kontrollerin luonti
 * kontrolleri = new PainoindeksiKontrolleri(this); }
 */