package model;

public class Valuuttakone implements IValuuttakone {

	Valuutta[] valuutat;
	Valuutta valuutta;
	IValuuttaDAO valuutatDAO = new ValuuttaAccessObject();
	
	public Valuuttakone() {
		valuutatDAO = new ValuuttaAccessObject();
		valuutat=valuutatDAO.readValuutat();
	};
		
	@Override
	public String[] getVaihtoehdot() {
		// TODO Auto-generated method stub
		String[] palautus = new String[valuutat.length];		
		for (int i = 0; i < valuutat.length; i++) {
		palautus[i] = valuutat[i].getNimi();
	}
		return palautus;
	}

	@Override
	public double muunna(int mistäIndeksi, int mihinIndeksi, double määrä) {
		// TODO Auto-generated method stub
		return (määrä/valuutat[mistäIndeksi].getVaihtokurssi())*(valuutat[mihinIndeksi].getVaihtokurssi());
		
		
	}


}
